

This is a code based on the method described in [this recent paper.](https://arxiv.org/abs/2105.06372) 

Running the script 'run_this_script.py' produces the time trace shown in Fig. 2d in the main text and in Fig. S1a in the supplementary information. 


Below is a description of the variables that appear in the script 'run_this_script.py'.

<h1>Section 1: Hamiltonian</h1>
We consider spinful Fermions on a lattice with L sites. The Hamiltonian we consider is 

<a href="https://www.codecogs.com/eqnedit.php?latex=H=&space;J\sum_{i=1,&space;\sigma=&space;\downarrow,&space;\uparrow}^L&space;\hat{c}_{i&plus;1,&space;\sigma}^{\dagger}&space;\hat{c}_{i,&space;\sigma}&space;&plus;&space;h.c&space;&plus;&space;\sum_{i=1,&space;\sigma=&space;\downarrow,&space;\uparrow}^L&space;V_{i,&space;\sigma}&space;\hat{c}_{i,&space;\sigma}^{\dagger}&space;\hat{c}_{i,&space;\sigma}&space;&plus;&space;U&space;\sum_{i=1}^L&space;\hat{c}_{i,&space;\uparrow}^{\dagger}&space;\hat{c}_{i,&space;\uparrow}\hat{c}_{i,&space;\downarrow}^{\dagger}&space;\hat{c}_{i,&space;\downarrow}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?H=&space;J\sum_{i=1,&space;\sigma=&space;\downarrow,&space;\uparrow}^L&space;\hat{c}_{i&plus;1,&space;\sigma}^{\dagger}&space;\hat{c}_{i,&space;\sigma}&space;&plus;&space;h.c&space;&plus;&space;\sum_{i=1,&space;\sigma=&space;\downarrow,&space;\uparrow}^L&space;V_{i,&space;\sigma}&space;\hat{c}_{i,&space;\sigma}^{\dagger}&space;\hat{c}_{i,&space;\sigma}&space;&plus;&space;U&space;\sum_{i=1}^L&space;\hat{c}_{i,&space;\uparrow}^{\dagger}&space;\hat{c}_{i,&space;\uparrow}\hat{c}_{i,&space;\downarrow}^{\dagger}&space;\hat{c}_{i,&space;\downarrow}" title="H= J\sum_{i=1, \sigma= \downarrow, \uparrow}^L \hat{c}_{i+1, \sigma}^{\dagger} \hat{c}_{i, \sigma} + h.c + \sum_{i=1, \sigma= \downarrow, \uparrow}^L V_{i, \sigma} \hat{c}_{i, \sigma}^{\dagger} \hat{c}_{i, \sigma} + U \sum_{i=1}^L \hat{c}_{i, \uparrow}^{\dagger} \hat{c}_{i, \uparrow}\hat{c}_{i, \downarrow}^{\dagger} \hat{c}_{i, \downarrow}" /></a>

We use an on-site potential of the form <a href="https://www.codecogs.com/eqnedit.php?latex=V_{i,&space;\sigma}=&space;\Delta_{\sigma}(i-L/2)&space;&plus;&space;\alpha&space;(i-L/2)^2&space;&plus;&space;\delta_{i}&space;&plus;&space;\Delta_{aa}\cos(2\pi&space;\beta&space;i&space;&plus;&space;\phi_{aa}&space;)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?V_{i,&space;\sigma}=&space;\Delta_{\sigma}(i-L/2)&space;&plus;&space;\alpha&space;(i-L/2)^2&space;&plus;&space;\delta_{i}&space;&plus;&space;\Delta_{aa}\cos(2\pi&space;\beta&space;i&space;&plus;&space;\phi_{aa}&space;)" title="V_{i, \sigma}= \Delta_{\sigma}(i-L/2) + \alpha (i-L/2)^2 + \delta_{i} + \Delta_{aa}\cos(2\pi \beta i + \phi_{aa} )" /></a>. Here, <a href="https://www.codecogs.com/eqnedit.php?latex=\delta_i&space;\in&space;[-\Delta_r,&space;\Delta_r]" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\delta_i&space;\in&space;[-\Delta_r,&space;\Delta_r]" title="\delta_i \in [-\Delta_r, \Delta_r]" /></a> is a random onsite potential.



1. J is the hopping rate (as it appears in the above Hamiltonian)
2. Delta_random = <a href="https://www.codecogs.com/eqnedit.php?latex=\Delta_{r}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\Delta_{r}" title="\Delta_{r}" /></a>
3. Delta_aubry_andre = <a href="https://www.codecogs.com/eqnedit.php?latex=\Delta_{aa}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\Delta_{aa}" title="\Delta_{aa}" /></a>
4. phi_aubry_andre= <a href="https://www.codecogs.com/eqnedit.php?latex=\phi_{aa}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\phi_{aa}" title="\phi_{aa}" /></a>
5. Delta_dn = <a href="https://www.codecogs.com/eqnedit.php?latex=\Delta_{\downarrow}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\Delta_{\downarrow}" title="\Delta_{\downarrow}" /></a>
6. Delta_up = <a href="https://www.codecogs.com/eqnedit.php?latex=\Delta_{\downarrow}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\Delta_{\uparrow}" title="\Delta_{\uparrow}" /></a>
7. U is as in the Hamiltonian.
8. ti is the initial time in milliseconds.
9. tf is the final time in milliseconds
10. n is the number of samples desired per millisecond. Total number of samples is m= int(n*(tf-ti)) +1. 
11. alpha = <a href="https://www.codecogs.com/eqnedit.php?latex=\alpha" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\alpha" title="\alpha" /></a> in the Hamiltonian above.



<h1>Section 2: System size and initial state</h1>
The variables that appear in this section are described below.

1.  L_apx = number of lattice sites. 
2.  doublon_fraction = Fraction of atoms bound in doublons. 
3.  hole_fraction = Fraction of even sites that are empty. 


<h1>Section 3:  Approximation parameters</h1>
The variables that appear in this section are described below.

1.  ell = ell in the approximate method.
2.  k_up = k_up in the approximate method. 
3.  k_dn = k_dn in the approximate method.
