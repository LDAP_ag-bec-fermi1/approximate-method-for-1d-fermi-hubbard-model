# -*- coding: utf-8 -*-
"""
Created on Mon Apr 26 14:47:42 2021

@author: Bharath.Hebbe
"""



import numpy as np
from scipy.linalg import expm, sinm, cosm
import time as tm
from scipy.special import comb
import random as rnd
import itertools
import matplotlib.pyplot as plt
import Exact_Diagonalization_for_apx as ED
import apx_combinatorial_functions as apx_config
import pickle


##############################INPUT PARAMETERS#################################################################################################################################################################

##################################################SYSTEM VARIABLES############################################################################################################################################

trotter_num1=100
trotter_num2=100


def Vandermonde_vec(X):
    Y=[]
    for j in range(len(X)):
        a=(-1)**(len(X)-1)
        for k in range(len(X)):
            if j!=k:
               a=a*X[k]/(X[j]-X[k]) 
        Y.append(a)
    return Y

def time_trace_dn(J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, Ulist0, Tr, gamma, ti, tf, n, ell, k_dn, k_up, k_db, L_full, Nup, Ndn, Ndb):
    #gamma=0
    #k_db=0
    L=2*ell+1
    alpha=1.0/(L_full*Tr)
    #DeltaRR=0.0*J				# Real random disorder strength in KHz
    #DeltaAA=0				# Aubry-Andre disorder strength in KHz
    #phiAA= 0	
    #L_full=280
    #Nup=L_full//4
    #Ndn=L_full//4
    #Ndb=0
    deltaU=0#2.5*J-1.0*Ulist0/20.0
    q_list, index_list=apx_config.generate_q_list(k_dn, k_up, k_db)
    P_up_final=np.zeros(( 1+int(n*(tf-ti)), L))
    P_dn_final=np.zeros(( 1+int(n*(tf-ti)), L))
    num_db_e_final=np.zeros(1+int(n*(tf-ti)))
    num_db_o_final=np.zeros(1+int(n*(tf-ti)))
    site_list=np.arange(-L_full//2, L_full//2, 2)
    for i_site in site_list:
        dDelta=2*alpha*i_site
        t_begin=tm.time()

        P_up_out=np.zeros(( 1+int(n*(tf-ti)), L))
        P_dn_out=np.zeros(( 1+int(n*(tf-ti)), L))
        norm_factor=0
        num_db_e=np.zeros(1+int(n*(tf-ti)))
        num_db_o=np.zeros(1+int(n*(tf-ti)))
        Ulist=Ulist0#+deltaU*(i_site-L_full/2.0)/(L_full)
    


        for i in range(len(index_list)):
            tstart=tm.time()
            i_config=index_list[i]
            q_vec=q_list[i_config]
            up_config, dn_config = apx_config.state_list_up(ell, k_dn, k_up, k_db, q_vec)
            n_up=len(up_config[0])
            n_dn=len(dn_config[0])
            #T2, NEUP2, NEDN2, NEDB2, NODB2=EDIP.tevol(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, 2*np.pi*ti, 2*np.pi*tf, int(n*(tf-ti)), trotter_num1/n)
            coeff=apx_config.external_coeff(L_full, k_dn, k_up, k_db, Ndn, Nup, Ndb, q_vec)
            if coeff>0:
                T, P_up, P_dn, NEDB, NODB=ED.tevol_multiple_config(L,n_up,n_dn, J, DeltaRR, DeltaAA, phiAA, DeltaS1+dDelta, DeltaS2+dDelta, alpha, up_config, dn_config, Ulist, gamma,  2*np.pi*ti, 2*np.pi*tf, int(n*(tf-ti)), int(trotter_num1/n))
    
                norm_factor=coeff*len(up_config)+norm_factor
                P_up_out=P_up_out+P_up*coeff/n_up
                P_dn_out=P_dn_out+P_dn*coeff/max(1, n_dn)
                if min(n_up, n_dn, k_db)>0:
                    num_db_e=num_db_e+NEDB*coeff/k_db#min(n_up, n_dn)
                    num_db_o=num_db_o+NODB*coeff/k_db#min(n_up, n_dn)
                #print(NEDB)
                #print(NODB)
        #print(coeff, n_up, n_dn)
        #print(tm.time()-tstart, i, len(index_list))
        time_str='%.2f' % (tm.time()-t_begin)
        print('step ' + str(i_site//2+L_full//4+1)+ ' of '+ str(L_full//2) +' took '+ time_str + ' seconds')
        P_up_out=P_up_out/norm_factor
        num_db_e=num_db_e/norm_factor
        num_db_o=num_db_o/norm_factor
        P_up_final=P_up_final+P_up_out/len(site_list)
        num_db_e_final=num_db_e_final+num_db_e/len(site_list)
        num_db_o_final=num_db_o_final+num_db_o/len(site_list)
    Imb=(-1)**np.arange(L)
    Imb_up=-np.dot(P_up_final, Imb)
    Imb_up=Imb_up*Imb_up[0]
    #imb_db_final=imb_db_final*imb_db_final[0]
    return T, Imb_up, num_db_e_final, num_db_o_final
    

def time_trace_db(J, DeltaS1, DeltaS2, Ulist0, Tr, gamma, ti, tf, n, ell, k_dn, k_up, k_db, L_full, Nup, Ndn, Ndb):
    #gamma=0
    #k_db=0
    L=2*ell+1
    DeltaRR=0.0*J				# Real random disorder strength in KHz
    DeltaAA=0				# Aubry-Andre disorder strength in KHz
    phiAA= 0	
    #L_full=280
    alpha=1.0/(L_full*Tr)
  
    deltaU=0#2.5*J-1.0*Ulist0/20.0
    q_list, index_list=apx_config.generate_q_list(k_dn, k_up, k_db)
    P_up_final=np.zeros(( 1+int(n*(tf-ti)), L))
    P_dn_final=np.zeros(( 1+int(n*(tf-ti)), L))
    num_db_e_final=np.zeros(1+int(n*(tf-ti)))
    num_db_o_final=np.zeros(1+int(n*(tf-ti)))
    site_list=np.arange(-L_full//2, L_full//2, 2)
    for i_site in site_list:
        dDelta=alpha*i_site
        t_begin=tm.time()

        P_up_out=np.zeros(( 1+int(n*(tf-ti)), L))
        P_dn_out=np.zeros(( 1+int(n*(tf-ti)), L))
        norm_factor=0
        num_db_e=np.zeros(1+int(n*(tf-ti)))
        num_db_o=np.zeros(1+int(n*(tf-ti)))
        Ulist=Ulist0+deltaU*(i_site-L_full/2.0)/(L_full)
    


        for i in range(len(index_list)):
            tstart=tm.time()
            i_config=index_list[i]
            q_vec=q_list[i_config]
            up_config, dn_config = apx_config.state_list_db(ell, k_dn, k_up, k_db, q_vec)
            n_up=len(up_config[0])
            n_dn=len(dn_config[0])
            #T2, NEUP2, NEDN2, NEDB2, NODB2=EDIP.tevol(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, 2*np.pi*ti, 2*np.pi*tf, int(n*(tf-ti)), trotter_num1/n)
            coeff=apx_config.external_coeff(L_full, k_dn, k_up, k_db, Ndn, Nup, Ndb, q_vec)
            if coeff>0:
                T, P_up, P_dn, NEDB, NODB=ED.tevol_multiple_config(L,n_up,n_dn, J, DeltaRR, DeltaAA, phiAA, DeltaS1+dDelta, DeltaS2+dDelta, alpha, up_config, dn_config, Ulist, gamma,  2*np.pi*ti, 2*np.pi*tf, int(n*(tf-ti)), int(trotter_num1/n))
    
                norm_factor=coeff*len(up_config)+norm_factor
                P_up_out=P_up_out+P_up*coeff/n_up
                P_dn_out=P_dn_out+P_dn*coeff/n_dn
                if min(n_up, n_dn)>0:
                    num_db_e=num_db_e+NEDB*coeff/(k_db+1)#(n_up+n_dn)#min(n_up, n_dn)
                    num_db_o=num_db_o+NODB*coeff/(k_db+1)#(n_up+n_dn)#min(n_up, n_dn)
        #print(coeff, n_up, n_dn)
        #print(tm.time()-tstart, i, len(index_list))
        print(tm.time()-t_begin, i_site)
        P_up_out=P_up_out/norm_factor
        num_db_e=num_db_e/norm_factor
        num_db_o=num_db_o/norm_factor
        P_up_final=P_up_final+P_up_out/len(site_list)
        num_db_e_final=num_db_e_final+num_db_e/len(site_list)
        num_db_o_final=num_db_o_final+num_db_o/len(site_list)
    Imb=(-1)**np.arange(L)
    Imb_up=-np.dot(P_up_final, Imb)
    Imb_up=Imb_up*Imb_up[0]
    #imb_db_final=imb_db_final*imb_db_final[0]
    return T, Imb_up, num_db_e_final, num_db_o_final


#######################INTERPOLATED#####################################################################


def elementary(X, k):
    n=len(X)
    X=np.array(X)
    p=0
    for subset in itertools.combinations(np.arange(n),k):
        s=list(subset)
        #print( X[s])
        p=p+np.product(X[s])
    return p


def Vandermonde_inv(X):
    n=len(X)
    V_in=np.zeros((n, n))
    for i in range(1, n+1):
        for j in range(1, n+1):
            V_in[i-1, j-1]=(-1)**(n-i)*elementary(np.delete(X, j-1), n-i)/np.product(np.delete(X-X[j-1], j-1))
    return V_in

def time_trace_up_interpolated(J, DeltaS1, DeltaS2, Ulist0, Tr, gamma, ti, tf, n, ell, k_dn, k_up, k_db, L_full, Nup, Ndn, Ndb):
    #gamma=0
    #k_db=0
    L_it=96
    L=2*ell+1
    alpha=1.0/(2*L_it*Tr)
    DeltaRR=0.0*J				# Real random disorder strength in KHz
    DeltaAA=0				# Aubry-Andre disorder strength in KHz
    phiAA= 0	
    #L_full=280
    #Nup=L_full//4
    #Ndn=L_full//4
    #Ndb=0
    deltaU=0#2.5*J-1.0*Ulist0/20.0
    q_list, index_list=apx_config.generate_q_list(k_dn, k_up, k_db)
    P_up_final=np.zeros(( 1+int(n*(tf-ti)), L))
    P_dn_final=np.zeros(( 1+int(n*(tf-ti)), L))
    num_db_e_final=np.zeros(1+int(n*(tf-ti)))
    num_db_o_final=np.zeros(1+int(n*(tf-ti)))
    interpol_num=5
    site_list=np.arange(-L_it//2, L_it//2, 2)
    Imb_interpol=[]
    x_interpol=[]
    for i_site in site_list:
        dDelta=alpha*i_site
        t_begin=tm.time()
        x_interpol.append(dDelta)

        P_up_out=np.zeros(( 1+int(n*(tf-ti)), L))
        P_dn_out=np.zeros(( 1+int(n*(tf-ti)), L))
        norm_factor=0
        num_db_e=np.zeros(1+int(n*(tf-ti)))
        num_db_o=np.zeros(1+int(n*(tf-ti)))
        Ulist=Ulist0#+deltaU*(i_site-L_full/2.0)/(L_full)
    


        for i in range(len(index_list)):
            tstart=tm.time()
            i_config=index_list[i]
            q_vec=q_list[i_config]
            up_config, dn_config = apx_config.state_list_up(ell, k_dn, k_up, k_db, q_vec)
            n_up=len(up_config[0])
            n_dn=len(dn_config[0])
            #T2, NEUP2, NEDN2, NEDB2, NODB2=EDIP.tevol(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, 2*np.pi*ti, 2*np.pi*tf, int(n*(tf-ti)), trotter_num1/n)
            coeff=apx_config.external_coeff(L_full, k_dn, k_up, k_db, Ndn, Nup, Ndb, q_vec)
            if coeff>0:
                T, P_up, P_dn, NEDB, NODB=ED.tevol_multiple_config(L,n_up,n_dn, J, DeltaRR, DeltaAA, phiAA, DeltaS1+dDelta, DeltaS2+dDelta, alpha, up_config, dn_config, Ulist, gamma,  2*np.pi*ti, 2*np.pi*tf, int(n*(tf-ti)), int(trotter_num1/n))
    
                norm_factor=coeff*len(up_config)+norm_factor
                P_up_out=P_up_out+P_up*coeff/n_up
                P_dn_out=P_dn_out+P_dn*coeff/n_dn
                if min(n_up, n_dn)>0:
                    num_db_e=num_db_e+NEDB*coeff/k_db#min(n_up, n_dn)
                    num_db_o=num_db_o+NODB*coeff/k_db#min(n_up, n_dn)
                #print(NEDB)
                #print(NODB)
        #print(coeff, n_up, n_dn)
        #print(tm.time()-tstart, i, len(index_list))
        print(tm.time()-t_begin, i_site)
        P_up_out=P_up_out/norm_factor
        num_db_e=num_db_e/norm_factor
        num_db_o=num_db_o/norm_factor
        P_up_final=P_up_final+P_up_out/len(site_list)
        num_db_e_final=num_db_e_final+num_db_e/len(site_list)
        num_db_o_final=num_db_o_final+num_db_o/len(site_list)
        Imb=(-1)**np.arange(L)
        Imb_up=-np.dot(P_up_out, Imb)#/len(site_list)
        Imb_up=Imb_up*Imb_up[0]
        Imb_interpol.append(Imb_up)
        
    
    Imb_interpol=np.array(Imb_interpol)
    
    #######################Interpolation
    
    imb1=np.mean(Imb_interpol, axis=0)
    imb2=np.mean(Imb_interpol[list(np.arange(0, L_it//2, 2)), :], axis=0)
    imb3=np.mean(Imb_interpol[list(np.arange(0, L_it//2, 3)), :], axis=0)
    imb4=np.mean(Imb_interpol[list(np.arange(0, L_it//2, 4)), :], axis=0)
    
    imb_interpol=np.zeros((4, len(imb1)))
    imb_interpol[0, :]=imb1
    imb_interpol[1, :]=imb2
    imb_interpol[2, :]=imb3
    imb_interpol[3, :]=imb4
    
    x_interpol=np.array([1/L_it, 2/L_it, 3/L_it, 4/L_it])
    V_in=Vandermonde_inv(x_interpol)
    
    y_interpol=np.array(Vandermonde_vec(x_interpol))
    
    #M_interpol_inv=np.zeros((2,2))
    #det=np.mean(x_interpol**2)-np.mean(x_interpol)**2
    #M_interpol_inv[0, 0]=np.mean(x_interpol**2)/det
    #M_interpol_inv[1, 0]=-np.mean(x_interpol)/det
    #M_interpol_inv[0, 1]=-np.mean(x_interpol)/det
    #M_interpol_inv[1, 1]=1/det
    
    #a_interpol=np.zeros((2, len(imb1)))
    #a_interpol[0, :]=np.mean(imb_interpol, axis=0)
    #a_interpol[1, :]=np.dot(x_interpol, imb_interpol)
    
    #_interpol_inv=np.linalg.inv(M_interpol)
    #imb_fit=np.dot(M_interpol_inv, a_interpol)
    
    
    #x_val=np.array([1, 1/L_full])
    #Imb_up=np.dot(x_val, imb_fit)
    
    Imb_up=2*imb1-imb2+ (imb2-imb1)*L_it/L_full
    
    imb_interpol_coeff=np.dot(V_in, imb_interpol)
    x_val=np.array([1, 1/L_full, 1/L_full**2, 1/L_full**3])
    
    Imb_up3=np.dot(x_val, imb_interpol_coeff)
    
    
    Imb_up2=np.dot(y_interpol, imb_interpol)
    #print(M_interpol_inv, det)
    
    
    #imb_db_final=imb_db_final*imb_db_final[0]
    return T, Imb_up, Imb_up2, Imb_up3
