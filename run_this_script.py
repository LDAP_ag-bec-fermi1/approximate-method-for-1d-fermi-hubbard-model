# -*- coding: utf-8 -*-
"""
Created on Sun May  9 14:15:43 2021

@author: Bharath.Hebbe
"""

import matplotlib.pyplot as plt
import numpy as np
import Time_trace_apx as tt_apx
import pickle


#############################################DYNAMICAL PARAMETERS##########################################################
J=0.543					# Tunneling rate in KHz
Delta_random=0   		# Random disorder strength in KHz
Delta_aubry_andre=0		# Aubry-Andre disorder strength in KHz
phi_aubry_andre= 0		# Aubry-Andre disorder phase
Delta_dn=1.8			# Stark tilt for spin down in KHz
Delta_up=1.62	        # Stark tilt for spin up in KHz
U=5*J					# Interaction in KHz
ti = 0					# Starting time in millisecond
tf = 300 				# Ending time in millisecond
n= 50					# Number of samples per millisecond
gamma=0                 #Doublon loss rate in kHz
Tr=10.0                 #Revival time in milliseconds
##############################SYSTEM AND INITIAL STATE PARAMETERS#################################################################################################################################################################

L_apx=280
doublon_fraction=0
hole_fraction=0
				
#####################################APPROXIMATION PARAMATERS###################################################################


ell=7
k_up=3
k_dn=0
k_db=0


#######################################################TIME EVOLUTION###########################################################################################################################

Ulist=U*np.ones(int(n*(tf-ti))+1)

Nu=L_apx/2*(1-hole_fraction)*((1-doublon_fraction)/(2-doublon_fraction))
Nd=L_apx/2*(1-hole_fraction)*((1-doublon_fraction)/(2-doublon_fraction))					
Nud=L_apx/2*(1-hole_fraction)*doublon_fraction/(2-doublon_fraction)

T, Imb_dn, num_db_e, num_db_o =tt_apx.time_trace_dn(J, Delta_random, Delta_aubry_andre, phi_aubry_andre,  Delta_dn, Delta_up, Ulist, Tr, gamma, ti, tf, n, ell, k_up, k_dn, k_db, L_apx, int(Nu), int(Nd), int(Nud))



###########################PLOTTING####################################################################################################
plt.figure(1)
plt.semilogx()
plt.plot(2*np.pi*J*T, Imb_dn, label='apx')
plt.xlabel(r'Time $t$ ($\tau$)')
plt.ylabel(r'$I^{\downarrow}$')
plt.legend()


filename='imbalance_time_trace.pic'
fit_file=open(filename, 'wb')
pickle.dump(2*np.pi*J*T, fit_file)
pickle.dump(Imb_dn, fit_file)
fit_file.close()


plt.show()
