# -*- coding: utf-8 -*-
"""
Created on Thu Mar 11 15:28:34 2021

@author: Bharath.Hebbe
"""

import numpy as np
import matplotlib.pyplot as plt
import itertools
from scipy.special import comb


def generate_partition_list(N, k):
    partitions=[]
    Mat=np.eye(k)-np.diagflat(np.ones(k-1), -1)
    vec=np.ones(k)
    vec[0]=0
    
    for subset in itertools.combinations(np.arange(N+k), k):
        x=np.array(subset)
        x=np.dot(Mat, x)-vec
        partitions.append(x)
    return partitions

def generate_q_list(kdn, kup, kdb):
    Y1=generate_partition_list(kdn-kup, 1)
    Y2=generate_partition_list(kup-kdb, 2)
    Y3=generate_partition_list(kdb, 3)
    n1, n2, n3=len(Y1), len(Y2), len(Y3)
    q_list=[]
    for_sorting=[]
    #print(n1, n2, n3)
    for i1 in range(n1):
        v1=Y1[i1]
        for i2 in range(n2):
            v2=Y2[i2]
            for i3 in range(n3):
                v3=Y3[i3]
                x=np.zeros(6)
                x[0]=v1
                x[1:3]=v2
                x[3:6]=v3
                q_list.append(x)
                #print(x)
                k1=v3[0]+v3[1]+v2[0]
                k2=v3[0]+v3[2]+v2[1]+v1[0]
                for_sorting.append(k1+10*k2)
    index_list=np.argsort(for_sorting)
    return q_list, index_list

def multinomial_combinataions(N, K):
    l=len(K)
    sizes=N-np.cumsum(K)
    sizes=np.concatenate((np.array([N]), sizes[:-1]))
   # print(sizes, "sizes")
    X=[]
    for j in range(l):
        S=itertools.combinations(np.arange(sizes[j]), K[j])
        X.append(S)
    Y0=[]
    for subset in X[0]:
        Y0.append([list(subset)])
    def add_layer(Y, N, N_new, k_new):
        full_set=set(list(np.arange(N)))
        k=len(Y)
        Y_out=[]
        for i in range(k):
            item=list(Y[i])
           # print(i, k)
            rem_set=full_set-set(sum(item, []))
            #print(rem_set)
            rem_set=np.sort(list(rem_set))
            for subset in itertools.combinations(np.arange(N_new), k_new):
                item=list(Y[i])
                new_set=list(rem_set[list(subset)])
                new_set=np.sort(new_set)
                item.append(list(new_set))
                #print(item, i, k)
                Y_out.append(item)
        return Y_out
    for layer in range(l-1):
        #print(layer, sizes[layer+1], K[layer+1])
        Y0=add_layer(Y0, N, sizes[layer+1], K[layer+1])

    #Y1=add_layer(Y0,  N, sizes[1], K[1])
    return Y0


def external_coeff(L, k_dn, k_up, k_db, Ndn, Nup, Ndb, q_vec):
    q_dn_o=q_vec[0]
    q_up_m=q_vec[1]
    q_dn_m=q_vec[2]
    q_db=q_vec[3]
    q_up_i=q_vec[4]
    q_dn_i=q_vec[5]
    q_up=q_up_i + q_up_m
    q_dn=q_dn_i + q_dn_m+ q_dn_o
    factor1=comb(L//2-k_dn, Ndn-q_dn)
    factor2=comb(L//2-k_up-(Ndn-q_dn+q_dn_o), Nup-q_up)
    factor3=comb(L//2-k_db-(Ndn-q_dn_i)-(Nup-q_up_i), Ndb-q_db)
    #print(factor1, factor2, factor3, L//2, -k_db-(Ndn-q_dn_i)-(Nup-q_up_i), Ndb-q_db)
    return factor1*factor2*factor3
                
def state_list(ell, k_dn, k_up, k_db, q_vec):
    S_up=[]
    S_dn=[]
    q_dn_o=int(q_vec[0])
    q_up_m=int(q_vec[1])
    q_dn_m=int(q_vec[2])
    q_db=int(q_vec[3])
    q_up_i=int(q_vec[4])
    q_dn_i=int(q_vec[5])
    C3= multinomial_combinataions(k_db, np.array([q_db, q_up_i, q_dn_i]))
    C2= multinomial_combinataions(k_up-k_db, np.array([ q_up_m, q_dn_m]))
    C1= multinomial_combinataions(k_dn-k_up, np.array([q_dn_o]))
    total_occ_sites=np.arange(-2*ell, 2*ell+1, 2)
    total_occ_sites=np.delete(total_occ_sites, ell)
    inn_occ_sites=total_occ_sites[ell-k_db//2:ell-k_db//2+k_db]
    mid_occ_sites=total_occ_sites[ell-k_up//2:ell-k_up//2+k_up]
    out_occ_sites=total_occ_sites[ell-k_dn//2:ell-k_dn//2+k_dn]
    mid_occ_sites=np.delete(mid_occ_sites, list(np.arange(k_up//2-k_db//2, k_up//2-k_db//2+k_db)))
    out_occ_sites=np.delete(out_occ_sites, list(np.arange(k_dn//2-k_up//2, k_dn//2-k_up//2+k_up)))
    #print(inn_occ_sites)
    #print(mid_occ_sites)
    #print(out_occ_sites)
    #print(total_occ_sites)
    for c1 in C1:
        dn_atm1=out_occ_sites[c1[0]]
        for c2 in C2:
            dn_atm2=np.concatenate((dn_atm1, mid_occ_sites[c2[1]]))
            up_atm2=mid_occ_sites[c2[0]]
            for c3 in C3:
                #print(c3)
                dn_atm3=np.concatenate((dn_atm2, inn_occ_sites[c3[0]]))
                dn_atm3=np.concatenate((dn_atm3, inn_occ_sites[c3[2]]))
                up_atm3=np.concatenate((up_atm2, inn_occ_sites[c3[0]]))
                up_atm3=np.concatenate((up_atm3, inn_occ_sites[c3[1]]))
                up_atm3=np.concatenate((up_atm3, np.array([0])))
                dn_atm3=np.concatenate((dn_atm3, np.array([0])))
                #print(up_atm3)
                #print(dn_atm3)
                S_up.append(ell+np.sort(up_atm3))
                S_dn.append(ell+np.sort(dn_atm3))
    
    return S_up, S_dn    


def state_list_up(ell, k_dn, k_up, k_db, q_vec):
    S_up=[]
    S_dn=[]
    q_dn_o=int(q_vec[0])
    q_up_m=int(q_vec[1])
    q_dn_m=int(q_vec[2])
    q_db=int(q_vec[3])
    q_up_i=int(q_vec[4])
    q_dn_i=int(q_vec[5])
    C3= multinomial_combinataions(k_db, np.array([q_db, q_up_i, q_dn_i]))
    C2= multinomial_combinataions(k_up-k_db, np.array([ q_up_m, q_dn_m]))
    C1= multinomial_combinataions(k_dn-k_up, np.array([q_dn_o]))
    total_occ_sites=np.arange(-2*ell, 2*ell+1, 2)
    total_occ_sites=np.delete(total_occ_sites, ell)
    inn_occ_sites=total_occ_sites[ell-k_db//2:ell-k_db//2+k_db]
    mid_occ_sites=total_occ_sites[ell-k_up//2:ell-k_up//2+k_up]
    out_occ_sites=total_occ_sites[ell-k_dn//2:ell-k_dn//2+k_dn]
    mid_occ_sites=np.delete(mid_occ_sites, list(np.arange(k_up//2-k_db//2, k_up//2-k_db//2+k_db)))
    out_occ_sites=np.delete(out_occ_sites, list(np.arange(k_dn//2-k_up//2, k_dn//2-k_up//2+k_up)))
    #print(inn_occ_sites)
    #print(mid_occ_sites)
    #print(out_occ_sites)
    #print(total_occ_sites)
    for c1 in C1:
        dn_atm1=out_occ_sites[c1[0]]
        for c2 in C2:
            dn_atm2=np.concatenate((dn_atm1, mid_occ_sites[c2[1]]))
            up_atm2=mid_occ_sites[c2[0]]
            for c3 in C3:
                #print(c3)
                dn_atm3=np.concatenate((dn_atm2, inn_occ_sites[c3[0]]))
                dn_atm3=np.concatenate((dn_atm3, inn_occ_sites[c3[2]]))
                up_atm3=np.concatenate((up_atm2, inn_occ_sites[c3[0]]))
                up_atm3=np.concatenate((up_atm3, inn_occ_sites[c3[1]]))
                up_atm3=np.concatenate((up_atm3, np.array([0])))
                #dn_atm3=np.concatenate((dn_atm3, np.array([0])))
                #print(up_atm3)
                #print(dn_atm3)
                S_up.append(ell+np.sort(up_atm3))
                S_dn.append(ell+np.sort(dn_atm3))
    
    return S_up, S_dn       
    

   
ell=5
k_dn=4
k_up=4
k_db=2
q_list, index_list=generate_q_list(k_dn, k_up, k_db)
q_vec=q_list[-3]
S_up, S_dn = state_list(ell, k_dn, k_up, k_db, q_vec)

#plt.figure(1)
#for point in Y:
#    plt.plot(point[0], point[2], '*k')
#plt.show()